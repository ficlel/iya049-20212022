const list = [
  {
    name: 'C',
    description: 'Lenguaje usado de manera didactica',
    img: './img/c_logo.png',
  },
  {
    name: 'C++',
    description: 'Lenguaje usado de manera didactica',
    img: './img/c_plus.png',
  },
  {
    name: 'Java',
    description: 'Usado para aprender las bases de OOP en todos los estudios, se han usado varios framworks web',
    img: './img/java_logo.png',
  },
  {
    name: 'C#',
    description: 'Primer lenguaje de programacion que he aprendido, framework MVC',
    img: './img/c_sharp_logo.png',
  },
  {
    name: 'PHP',
    description: 'Lenguaje de programacion usado en trabajos con framework Laravel',
    img: './img/php_logo.png',
  },
  {
    name: 'GIT',
    description: 'Herramienta de control de versiones utilizada en trabajos y estudios',
    img: './img/git_logo.png',
  },
  {
    name: 'Python',
    description: 'Lenguaje usado para resolver algoritmos complejos y para diversion, Flask a sido usado para BackEnd',
    img: './img/python_logo.png',
  },
  {
    name: 'Javascript',
    description: 'Lenguaje de proramacion web usada para trabajo, se usan frameworks como Vue, React, Svelte',
    img: './img/javascript_logo.png',
  },
  {
    name: 'Typescript',
    description: 'Interprete de javascript usado para tipos cuando los requerimientos lo necesitaban',
    img: './img/typescript_logo.png',
  },
  {
    name: 'MySQL',
    description: 'Gestor de base de datos preferido para bases de datos relacionales',
    img: './img/mysql_logo.png',
  },
  {
    name: 'MongoDB',
    description: 'Motor de base de datos preferido para base de datos no relaciones orientadas a documentos',
    img: './img/mongodb_logo.png',
  },
  {
    name: 'Docker',
    description: 'Gestor de contenedores usado para proyectos y despliegue de tecnologias',
    img: './img/docker_logo.png',
  },
  {
    name: 'Graphql',
    description: 'Lenguaje de consulta utilizada en trabajos',
    img: './img/graphql_logo.png',
  },
  {
    name: 'Vim',
    description: 'Editor de texto utilizado actualmente para manejar archivos',
    img: './img/vim_logo.png',
  },
];

const printTechList = (myList = []) => {
  const container = document.getElementsByClassName('container')[0];
  let item;
  let itemInformation;
  let itemImg;
  let itemDescription;
  let itemName; 
  myList.forEach((tech) => {
    item = document.createElement('div');  
    item.className = 'item';

    itemImg = document.createElement('img');
    itemImg.className = 'item-img';
    itemImg.src = tech.img;

    itemInformation = document.createElement('div');
    itemInformation.className = 'information';

    itemName = document.createElement('h3');
    itemName.innerText = tech.name;

    itemInformation.append(itemName);

    itemDescription = document.createElement('p');
    itemDescription.innerText = tech.description;

    itemInformation.append(itemDescription);

    item.append(itemImg);
    item.append(itemInformation);
    container.append(item);
  });
};

window.onload = () => {
  printTechList(list); 
};
