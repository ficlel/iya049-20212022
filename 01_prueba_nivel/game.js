let listCards = [
  {
    id: 1,
    type: 'diamond_as',
    img: 'img/as_diamantes.jpeg',
  },
  {
    id: 2,
    type: 'diamond_as',
    img: 'img/as_diamantes.jpeg',
  },
  {
    id: 3,
    type: 'pica_as',
    img: 'img/as_picas.png',
  },
  {
    id: 4,
    type: 'pica_as',
    img: 'img/as_picas.png',
  },
];

// Variable to control state of the pairing
let firts;
// count pair I know is hardcoded
let winning = 0;

let waiter = false;


function actionClick() {
  if (!waiter) {
    this.classList.toggle('is-flipped');
    verifyPairingValue(this);
  }
}

const shuffleArray = (array) => {
  for (let i = array.length - 1; i > 0; i-=1) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

const removeListeners = (card1, card2) => {
  console.log(card1);
  card1.removeEventListener('click', actionClick, true); 
  card2.removeEventListener('click', actionClick, true);
  firts = undefined;
};

const flickBack = (card1, card2) => {
  setTimeout(() => {
    card1.classList.toggle('is-flipped');
    card2.classList.toggle('is-flipped');
    firts = undefined;
  }, 1500);
}

const verifyWinnir = () => {
  if (winning === 2) {
    let local = sessionStorage.getItem('resultados');
    if (local && local !== null) {
      local = JSON.parse(local);
      sessionStorage.setItem('resultados', JSON.stringify([
        ...local,
        new Date().toLocaleString('en-UE', {timeZone: 'Europe/Madrid'}),
      ]));
    } else {
      sessionStorage.setItem('resultados', JSON.stringify([new Date()]));
    }
  } 
};

const verifyPairingValue = (value) => {
  waiter = true;
  if (!firts) {
    firts = value;
    waiter = false;
  } else {
    if (firts.dataset.type === value.dataset.type && firts.dataset.id !== value.dataset.id) {

      removeListeners(firts, value);
      winning += 1;
      verifyWinnir();
      waiter = false;
      // early return just because i dont want an else
      return;
    } 
    flickBack(firts, value);
    waiter = false;
  }
};
/*
<div class="card">
    <div class="card__face card__face--front">front</div>
    <div class="card__face card__face--back">back</div>
  </div>
*/
const printCards = (array = []) => {
  const container = document.getElementsByClassName('card-container')[0];
  let card;
  let front;
  let front_image;
  let back_image;
  let back;
  array.forEach((element) => {
    card = document.createElement('div');
    card.classList.add('card');
    card.dataset.type = element.type;
    card.dataset.id = element.id;

    back = document.createElement('div');
    back.classList.add('card_face');
    back.classList.add('back');
    back_image = document.createElement('img');
    back_image.className = 'item-image';
    back_image.src = element.img;
    back.append(back_image);
    

    front = document.createElement('div');
    front.classList.add('card_face');
    front.classList.add('front');
    front_image = document.createElement('img');
    front_image.className = 'item-image';
    front_image.src = './img/back_part.png';
    front.append(front_image);


    card.append(front);
    card.append(back);
    
    container.append(card);
  });
};

const initCardsLoad = () => {
  // Shuffle Array by memory reference
  shuffleArray(listCards);
  printCards(listCards);
  const cards = document.getElementsByClassName('card');
  console.log(cards);
  for (let card of cards) {
    card.addEventListener('click', actionClick, true);
  }
  
};

window.onload = () => {
  initCardsLoad();
};

