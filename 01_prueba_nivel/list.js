const getGameList = () => {
  let data = sessionStorage.getItem('resultados');
  const container = document.getElementsByClassName('container')[0];
  const ul = document.createElement('ul');
  ul.className = 'ul';
  let li;
  if (data && data !== null) {
    data = JSON.parse(data);  
    data.forEach((element) => {
      li = document.createElement('li'); 
      li.innerText = element.replace('Z', ' ').replace('T', ' ').substring(0, 19);
      ul.append(li);
    });
    container.append(ul);
  }
};

window.onload = () => {
  getGameList();
};
